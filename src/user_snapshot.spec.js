const user = require('./user');

describe('user snapshot test', function() {
  it('error when snapshot dont match', function() {
    expect(user(1)).toMatchSnapshot();
    expect(user(56)).toMatchSnapshot();
    expect(user(1345)).toMatchSnapshot();
  });
});