const randomPlus = require('./randomPlus');
const random = require('./random');

jest.mock('./random');

random.mockReturnValueOnce(50);

describe('randomPlus', function() {    
  it ('50 + 50 (random) = 100', function() {
    expect(randomPlus(50)).toBe(100);
  });
});