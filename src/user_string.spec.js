const user = require('./user');

describe('user entered string', function() {
  it('error when input is string', function() {
    expect(function() {
      user('cat'); 
    }).toThrow('id needs to be integer');
  });
});